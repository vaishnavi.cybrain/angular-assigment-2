import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  template: `<h2>Interpolation </h2>
  <h2> Hello {{name}}</h2>
            <h2>{{4+4}}</h2>
            <h2>class Binding</h2>
            <h2 class="text-success">All right</h2> 
            <h2>property binding</h2>
            <input [id]="myID" type ="text" value="Hello Everyone">
            <input [disabled]="false" id="{{myID}}" type="text" value="vaishnavi">
            <h2 class="text-special"[class]="successClass">Motivate</h2>
            <h2 [class.text-danger]="hasError">Motivate</h2>
            <h2>Event Binding</h2>
            <button (click)="onClick()">Greet</button>
            
             <button (click)="onClick()">All well</button>
             <button (click)="greeting='Good Afternoon'">Greet </button>
             {{greeting}}
             <h2>two way binding</h2>
             <input [(ngModel)]="name" type="text">
             {{name}}
              `,
              
             styles: [`
  .text-success{
    color:  blue;
  }
.text-danger{
  color: red;
}
.text-special{
  font-style: italic;
}


  `]
})
export class DatabindingComponent implements OnInit{
  public name="";
  public myID="bindingID";
  public successClass="text-success";
  public hasError=false;
  public isSpecial=true;
  public greeting="";
  public all="";

 constructor(){}
 ngOnInit() {
   }

   logMessage(){
    console.log();
   }
   onClick(){
    console.log('hy click here')
    this.greeting="Hy how are you";
   }
}
