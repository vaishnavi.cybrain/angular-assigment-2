import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  template: `
  <h2>ngIf</h2>
  <div *ngIf="displayName; then thenBlock; else elseBlock"></div>
  <h2 *ngIf="displayName">Warm
  </h2>
  <ng-template #thenBlock>
  <h2>AllGood</h2>
  </ng-template>

  <ng-template #elseBlock>
  <h2>Hidden</h2>
  </ng-template>
<h2>ngFor</h2>
  <div *ngFor="let shift of shifts">
  <div [style.background]="shift.color">{{shift.color}}</div>
 
  </div>
  <h2>ngIf and ngFor
  <div *ngFor="let items of items">

 <h2>{{items}}</h2>
  </div>
  <div *ngFor="let type of types">

  <h2>{{type}}</h2>
   </div>
   <div *ngFor="let car of cars">
  <div>{{car.id}}-{{car.name}}</div>
  
  `,
  styles: []
})
export class DirectivesComponent implements OnInit {
   shifts :any=[{color:"red"},
   {color:"blue"},
    {color:"green"},
     {color:"yellow"}];

     items: any=["fruits", "vegetables", "clothes"];
     types: any=["papaya", "jackfruit", "Orange"];
     cars: any=[
      {
        id:1,name:'Honda'
      },
      {
        id:2,name:'Hyundai'
      },
      {
        id:3,name:'Audi'
      }
     ]
  displayName=true;

   constructor(){ }

   ngOnInit(): void {
     
   }
}
