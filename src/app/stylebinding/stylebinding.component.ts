import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stylebinding',
  template: `
  <h2>ngstyle</h2><h2 [style.color]="hasError ? 'red' : 'green'">Style Binding</h2>
  
             <h2 [style.color]="highlightcolor">If you are a good listener or adviser, you can always give people good advice and listen to their problems. Helping others is the best thing you can do in life. Till the time we could not help others selflessly, there is no chance that we will be able to save the humanity.</h2> 
             <h2 [ngStyle]="">Style Binding 3</h2>
            <h2 [ngStyle]="titleStyles">Style Binding 3</h2>
            <h2>ngclass</h2>
            <h2 class="text-success">Keep good</h2>
            
            <h2 [class.text-danger]="hasError">Woah</h2>
            <h2 [ngClass]="messageClasses">Take Care</h2>
             `,

  styleUrls: []
})
export class StylebindingComponent implements OnInit {
public hasError =true;
public successClass="text-success";
public isSpecial=true;
public highlightcolor="orange";
public titleStyles={
  color:"blue",
  fontStyle:"italic"
}
public messageClasses={
  "text-success":!this.hasError,
  "text-danger":this.hasError,
  "text-special":this.isSpecial
}
constructor(){}


ngOnInit(){

}

}
